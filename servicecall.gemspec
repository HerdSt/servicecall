$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "servicecall/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "servicecall"
  spec.version     = Servicecall::VERSION
  spec.authors     = ["Matthew Mills"]
  spec.email       = ["matthew@herd.st"]
  spec.homepage    = "https://cognise.com"
  spec.summary     = "Caller other services."
  spec.description = "Caller other services."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = ""
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files = Dir["lib/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 6.0.3", ">= 6.0.3.2"
  spec.add_dependency "jwt", "~> 2.2"
  
  spec.add_development_dependency "sqlite3"
end
