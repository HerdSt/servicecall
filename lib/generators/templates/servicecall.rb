# Configures service call
Servicecall.setup do |config|

    config.jwt_secret "secret"
    
    config.register_service :service_one, "http://localhost:3000"
    config.register_service :service_two, "http://localhost:3001"
    config.register_service :service_three, "http://localhost:3002"
    
    config.on("user_created", "user#created")

end
