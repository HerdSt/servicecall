require 'rails/generators/base'


module Servicecall
    class InstallGenerator < Rails::Generators::Base
        source_root File.expand_path("../../templates", __FILE__)
        
        def create_initializer_file
            template "servicecall.rb", "config/initializers/servicecall.rb"
        end
    end
end
