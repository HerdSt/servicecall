require 'jwt'
require 'json'

require "servicecall/railtie"
require "servicecall/configurator"
require "servicecall/caller"
require "servicecall/receiver"
require "servicecall/middleware"
require "servicecall/request"
require "servicecall/response"
require "servicecall/responder"
require "servicecall/responder_error"
require "servicecall/mocker" if (ENV["RAILS_ENV"] == "test") || (ENV["WORKER_ENV"] == "test")


module Servicecall
    
    
    @@config = Servicecall::Configurator.new
    @@caller = Servicecall::Caller.new(@@config)
    @@receiver = Servicecall::Receiver.new(@@config)
    
    
    def self.config
        @@config
    end

    
    def self.receive(token, data)
        @@receiver.receive(token, data)
    end
    
    
    def self.call(service_name, event_name, data)
        @@caller.call(service_name, event_name, data)
    end
    
    
    def self.setup(&block)
        yield @@config if block_given?
    end
    
    
end
