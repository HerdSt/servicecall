module Servicecall
    class Request
    
        
        attr_accessor :event, :body
    

        def initialize(event, body)
            self.event = event
            self.body = body ? 
                (body.respond_to?(:with_indifferent_access) ? body.with_indifferent_access : body) : 
                nil
        end
        
        
        def to_json
            {
                :event => self.event,
                :data => self.body
            }.to_json
        end
        
        
        def parse!(data)
            body = JSON.parse(data) rescue nil
            
            raise "Invalid body" unless body
            
            self.event = body["event"]
            self.body = body["data"].respond_to?(:with_indifferent_access) ? 
                body["data"].with_indifferent_access : 
                body["data"]
        end
        
            
    end
end
