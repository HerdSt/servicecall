module Servicecall
    class Responder
    
    
        def initialize(event, data)
            @event = event
            @data = data
        end
    
        
        def reject!(data, code = 400)
            raise ResponderError.new(@event, data, code)
        end
        
        
    end
end
