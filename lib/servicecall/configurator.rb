module Servicecall
    class Configurator
    
        
        def initialize
            @config = Hash.new.with_indifferent_access
            @config[:services] = Hash.new
            @config[:events] = Hash.new
            @config[:event_path] = "/servicecall/event"
            @config[:jwt_secret] = nil
            @config[:jwt_algorithm] = "HS256"
        end
        
        
        def register_service(name, endpoint)
            @config[:services][name.to_sym] = endpoint
        end
        
        
        def get_service(name)
            @config[:services][name.to_sym]
        end
        
        
        def on(event_name, action)
            @config[:events][event_name.to_sym] = action
        end
        
        
        def method_missing(method, *args)
            if (args.size == 1)
                @config[method.to_sym] = args.first
            else
                @config[method.to_sym]
            end
        end
        
    
    end
end
