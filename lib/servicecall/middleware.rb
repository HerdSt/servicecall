module Servicecall
    class Middleware
    
    
        def initialize(app)
            @app = app
            @event_path = Servicecall.config.event_path
        end
        
        
        def call(env)
            if env["REQUEST_METHOD"] == "POST"
                req = nil
                
                unless env["REQUEST_PATH"]
                    req = Rack::Request.new(env)
                    
                    env["REQUEST_PATH"] = req.path
                end
                
                if env["REQUEST_PATH"] == @event_path
                    req = Rack::Request.new(env) unless req
                    body = req.body.read
                    token = Rack::Utils.parse_nested_query(req.query_string)["token"] rescue nil
                    
                    if body
                        response = Servicecall.receive(token, body)
                        
                        return [
                            response.code,
                            { "Content-Type" => "application/json" },
                            [ response.to_json ]
                        ]
                    end
                end
            end
            
            return @app.call(env)
        end
        
    
    end
end
