module Servicecall
    class ResponderError < Exception
        
        
        def initialize(event, data, code = 400)
            @event = event
            @code = code
            @response = Response.new(event, code, get_body(data))
        end
        
        
        def event
            @event
        end
        
        
        def code
            @code
        end
        
        
        def response
            @response
        end
        
        
        private
            def get_body(data)
                body = data
                
                if data.is_a?(String)
                    body = {
                        :message => data
                    }
                end
                
                if data.is_a?(ActiveModel::Validations) && ((data.errors.empty? == false) || (data.valid? == false))
                    body = {
                        :message => "A validation error occured", 
                        :errors => data.errors 
                    }
                end
                
                body
            end
        
    end
end
