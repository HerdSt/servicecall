module Servicecall
  class Railtie < ::Rails::Railtie
      
        
        initializer "servicecall.configure_rails_initialization" do |app|
            app.middleware.insert_after Rack::TempfileReaper, Servicecall::Middleware
        end
        
        generators do
            generator_path = File.expand_path("../../generators/servicecall", __FILE__)
            
            require "#{generator_path}/install_generator"
        end
        
        
  end
end
