require "minitest/mock"


module Servicecall
    class Mocker
        
        
        def self.mock_call(event, body, code = 200, &block)
            stub_call(:call, event, body, code) do
                yield if block_given?
            end
        end
        
        
        private
            def self.stub_call(method, event, body, code = 200)
                calls = []
                
                Servicecall::Caller.class_eval do
                    alias_method :"new_#{method}", method
                    
                    define_method(method) do |*args|
                        calls << args
                        
                        Servicecall::Response.new(event, code, body)
                    end
                end
                
                begin
                    yield if block_given?
                ensure
                    Servicecall::Caller.class_eval do
                        undef_method method
                        alias_method method, :"new_#{method}"
                        undef_method :"new_#{method}"
                    end
                end
                
                calls
            end
        
        
    end
end
