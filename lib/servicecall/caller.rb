require 'net/http'
require 'uri'


module Servicecall
    class Caller
    
        
        attr_accessor :config, :responder
        
        
        def initialize(config)
            self.config = config
        end
        
        
        def call(service_name, event_name, data)
            service_endpoint = @config.get_service(service_name.to_sym)
            
            raise "No service endpoint!" unless service_endpoint
            
            call_endpoint = service_endpoint + @config.event_path  + "?" + { :token => get_token }.to_query
            
            response = send_request!(call_endpoint, Servicecall::Request.new(event_name, data))
            
            Servicecall::Response.parse(response)
        end
        
        
        private
            def send_request!(endpoint, service_request)
                uri = URI.parse(endpoint)
                http = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == "https")
                request = Net::HTTP::Post.new(uri.to_s)
                
                request.content_type = "application/json"
                request.body = service_request.to_json
                
                make_request!(http, request)
            end
            
            def get_token
                payload = {
                    :iat => (Time.now.utc.to_i - 1),
                    :nbf => (Time.now.utc.to_i - 10),
                    :exp => (Time.now.utc.to_i + 300)
                }
                
                JWT.encode(payload, self.config.jwt_secret, self.config.jwt_algorithm)
            end
            
            def make_request!(http, request)
                http.request(request)
            end
        
            
    end
end
