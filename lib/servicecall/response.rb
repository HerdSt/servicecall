module Servicecall
    class Response
    
        
        attr_accessor :event, :body, :code
    

        def initialize(event, code, body)
            self.event = event
            self.code = code
            self.body = body ? 
                (body.respond_to?(:with_indifferent_access) ? body.with_indifferent_access : body) : 
                nil
        end
        
        
        def was_successful?
            code && code >= 200 && code < 400
        end
        
        
        def error(default = "Unknown Error")
            if (self.was_successful? == false)
                return (self.body == nil) ? { :message => default } : self.body
            end
            
            return nil
        end
        
        
        def to_json
            {
                :event => self.event,
                :success => self.was_successful?,
                :data => self.body
            }.to_json
        end
        
        
        def [](key)
            self.body ? self.body[key] : nil
        end
            
        
        def self.parse(response)
            body = JSON.parse(response.body) rescue nil
            event = body && body["event"]
            data = body && body["data"]
            
            self.new(event, response.code.to_i, data)
        end
        
            
    end
end
