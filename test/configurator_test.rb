require 'test_helper'

class Servicecall::ConfiguratorTest < ActiveSupport::TestCase
    
    
    test "Ensure we can create a configurator instance" do
        config = Servicecall::Configurator.new
    
        assert (config.instance_of?(Servicecall::Configurator) == true)
        assert (get_config(config)[:services].is_a?(Hash) == true)
        assert (get_config(config)[:services].size == 0)
        assert (get_config(config)[:events].is_a?(Hash) == true)
        assert (get_config(config)[:events].size == 0)
        assert (get_config(config)[:event_path] == "/servicecall/event")
        assert (get_config(config)[:jwt_secret] == nil)
        assert (get_config(config)[:jwt_algorithm] == "HS256")
    end
    
    
    test "Ensure we can register a service" do
        config = Servicecall::Configurator.new
        key = :power
        host = "https://localhost:666"
    
        config.register_service(key, host)
    
        assert (get_config(config)[:services].size == 1)
        assert (get_config(config)[:services][key] == host)
    end
    
    
    test "Ensure we can get a service" do
        config = Servicecall::Configurator.new
        key = :power
        host = "https://localhost:666"
    
        config.register_service(key, host)
    
        assert (config.get_service(key) == host)
    end
    
    
    test "Ensure we can add an event" do
        config = Servicecall::Configurator.new
        event = "user_get"
        action = "user#get"
    
        config.on(event, action)
    
        assert (get_config(config)[:events].size == 1)
        assert (get_config(config)[:events][event] == action)
    end
    
    
    test "Ensure we can get config instance variables" do
        config = Servicecall::Configurator.new
        
        config.register_service(:power, "https://localhost:666")
        config.on("user_get", "user#get")
        
        assert (config.services.is_a?(Hash) == true)
        assert (config.services.size == 1)
        assert (config.events.is_a?(Hash) == true)
        assert (config.events.size == 1)
    end
        
    
    private
        def get_config(instance)
            instance.instance_variable_get("@config")
        end
    
    
end
