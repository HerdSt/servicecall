require 'test_helper'

class Servicecall::ServicecallTest < ActionDispatch::IntegrationTest
    
    
     # Reset config
    setup do
        reset
    end
    
    teardown do
        reset
    end
    
    
    test "Ensure we can get the config from the module" do
        config = Servicecall.config
        
        assert (config != nil)
        assert (config.instance_of?(Servicecall::Configurator) == true)
        assert (config.services.size == 1)
        assert (config.services[:test] == "http://example.com")
        assert (config.events.size == 1)
        assert (config.events[:user_get] == "user#get")
        assert (config.jwt_secret == "[-h9uhm8+B7\HU/k")
    end
    
    
    test "Ensure we can receive an event" do
        receiver = Servicecall::Receiver.new(Servicecall.config)
        request_data = Servicecall::Request.new(get_event, { :key => "value" }).to_json
        response = Servicecall.receive(get_jwt_token(receiver.config), request_data)
    
        assert (response.instance_of?(Servicecall::Response) == true)
        assert (response.was_successful? == true)
        assert (response.body != nil)
        assert (response.body[:user] != nil)
        assert (response.body[:user]["first_name"] == "John")
        assert (response.body[:user]["last_name"] == "Smith")
        assert (response.body[:user]["key"] == "value")
    end
    
    
    test "Ensure we can call an event" do
        caller = Servicecall::Caller.new(Servicecall.config)
        response = nil
        
        mock_caller(caller) do |stubbed|
            response = stubbed.call(:test, get_event, { :key => "value" })
        end
        
        assert (response.instance_of?(Servicecall::Response) == true)
        assert (response.was_successful? == true)
        assert (response.body[:user] != nil)
        assert (response.body[:user]["first_name"] == "John")
        assert (response.body[:user]["last_name"] == "Smith")
        assert (response.body[:user]["key"] == "value")
    end
    
    
    test "Ensure we can setup the module" do
        jwt_secret = "dont_show_anyone"
        service_name = :new
        service_host = "http://localhost:5000"
        event_name = "user_update"
        event_action = "user#update"
    
        Servicecall.setup do |config|
            config.jwt_secret jwt_secret
            config.register_service service_name, service_host
            config.on(event_name, event_action)
        end
    
        assert (Servicecall.config.jwt_secret == jwt_secret)
        assert (Servicecall.config.services.size == 2)
        assert (Servicecall.config.services[service_name] == service_host)
        assert (Servicecall.config.events.size == 2)
        assert (Servicecall.config.events[event_name] == event_action)
    end
    
    
    private
        def reset
            Servicecall.config.jwt_secret jwt_secret
            Servicecall.config.services({ :test => "http://example.com" })
            Servicecall.config.events({ "user_get" => "user#get" })
        end
    
    
end
