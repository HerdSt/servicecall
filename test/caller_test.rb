require 'test_helper'


class Servicecall::CallerTest < ActionDispatch::IntegrationTest
    
    
    test "Ensue we can create a new instance" do
        caller = new_instance
    
        assert (caller.instance_of?(Servicecall::Caller) == true)
    end
    
    
    test "Ensue we can call a service" do
        caller = new_instance
        response = nil
        
        mock_caller(caller) do |stubbed|
            response = stubbed.call(:test, get_event, { :key => "value" })
        end
        
        assert (response.instance_of?(Servicecall::Response) == true)
        assert (response.was_successful? == true)
        assert (response.body[:user] != nil)
        assert (response.body[:user]["first_name"] == "John")
        assert (response.body[:user]["last_name"] == "Smith")
        assert (response.body[:user]["key"] == "value")
    end
    
    
    private
        def new_instance
            config = Servicecall::Configurator.new
            config.jwt_secret(jwt_secret)
            config.register_service(:test, "#{self.url_options[:protocol]}://#{self.url_options[:host]}")
            config.on(get_event, "user#get")
            
            Servicecall::Caller.new(config)
        end
    
    
end
