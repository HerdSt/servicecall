require 'net/http'

class CallResponse < Net::HTTPResponse
    
    def initialize(code, body)
        super(1.1, code, (code > 400 ? "Bad Request" : "OK"))
        
        self.body = body
    end
    
    def read_body
        @body
    end
    
end
