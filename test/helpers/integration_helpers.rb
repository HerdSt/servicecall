class ActionDispatch::IntegrationTest
    
    
    def mock_caller(caller, &block)
        caretaker = Proc.new do |http, request|
            post request.path, :params => request.body
            
            CallResponse.new(response.status, response.body)
        end
        
        caller.stub :make_request!, caretaker do |stubbed|
            yield stubbed if block_given?
        end
    end
    
    
end
