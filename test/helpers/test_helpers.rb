class ActiveSupport::TestCase
    
    def jwt_secret
        "[-h9uhm8+B7\HU/k"
    end
    
    def get_jwt_token(config)
        payload = {
            iat: (Time.now.utc.to_i - 1),
            nbf: (Time.now.utc.to_i - 60),
            exp: (Time.now.utc.to_i + 300)
        }
        
        JWT.encode(payload, config.jwt_secret, config.jwt_algorithm)
    end
    
    def get_event
        "user_get"
    end
    
    def get_body
        {
            :key => "value"
        }
    end
    
end
