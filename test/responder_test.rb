require 'test_helper'

class Servicecall::ResponderTest < ActiveSupport::TestCase
    

    test "Ensure we can create a new responder" do
        event = get_event
        data = { :key => "value" }
        responder = Servicecall::Responder.new(event, data)
        
        assert (responder.instance_variable_get("@event") == event)
        assert (responder.instance_variable_get("@data").is_a?(Hash) == true)
        assert (responder.instance_variable_get("@data")[:key] == "value")
    end
    
    
    test "Ensure we can reject a from with a responder" do
        event = get_event
        data = { :key => "value" }
        responder = Servicecall::Responder.new(event, data)
        
        assert_raises Servicecall::ResponderError do
            responder.reject!(data)
        end
    end
    
    
end
