require 'test_helper'

class Servicecall::ResponderErrorTest < ActiveSupport::TestCase
    
    
    test "Ensure we can create a responder error with a string" do
        event = get_event
        data = "Serious error"
        error_one = Servicecall::ResponderError.new(event, data, 403)
    
        assert (error_one.instance_of?(Servicecall::ResponderError) == true)
        assert (error_one.event == event)
        assert (error_one.code == 403)
        assert (error_one.response.instance_of?(Servicecall::Response) == true)
        assert (error_one.response.body != nil)
        assert (error_one.response.body[:message] == data)    
    end
    
    
    test "Ensure we can create a responder error with a hash" do
        event = get_event
        data = { :key => "value" }
        error_one = Servicecall::ResponderError.new(event, data)
    
        assert (error_one.instance_of?(Servicecall::ResponderError) == true)
        assert (error_one.event == event)
        assert (error_one.code == 400)
        assert (error_one.response.instance_of?(Servicecall::Response) == true)
        assert (error_one.response.body != nil)
        assert (error_one.response.body[:key] == "value")
    end
    
    
    test "Ensure we can create a responder error with a invalid model" do
        event = get_event
        user = User.new("john", "")
        error_one = Servicecall::ResponderError.new(event, user)
        
        assert (error_one.instance_of?(Servicecall::ResponderError) == true)
        assert (error_one.event == event)
        assert (error_one.code == 400)
        assert (error_one.response.instance_of?(Servicecall::Response) == true)
        assert (error_one.response.body != nil)
        assert (error_one.response.body[:message] == "A validation error occured")
        assert (error_one.response.body[:errors].is_a?(ActiveModel::Errors) == true)
    end
    
    
end
