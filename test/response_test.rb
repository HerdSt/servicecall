require 'stringio'
require 'test_helper'

class Servicecall::ResponseTest < ActiveSupport::TestCase
    
    
    test "Ensure we can create a response" do
        event = get_event
        code = 200
        body = { :key => "value" }
        response = Servicecall::Response.new(event, code, body)
    
        assert (response.event == event)
        assert (response.code == code)
        assert (response.body != nil)
        assert (response.body[:key] == "value")
        assert (response.body["key"] == "value")
    end
    
    
    test "Ensure we can tell if a response is successful" do
        success_response = Servicecall::Response.new(get_event, 200, nil)
        error_response = Servicecall::Response.new(get_event, 400, nil)
    
        assert (success_response.was_successful? == true)
        assert (error_response.was_successful? == false)
    end
    
    
    test "Ensure we can get the response error" do
        success_response = Servicecall::Response.new(get_event, 200, nil)
        assert (success_response.error(nil) == nil)
    
        error_one_response = Servicecall::Response.new(get_event, 400, nil)
        error_one = error_one_response.error("default")
    
        assert (error_one != nil).inspect
        assert (error_one[:message] == "default")
    
        error_two_response = Servicecall::Response.new(get_event, 400, { :errors => [] })
        error_two = error_two_response.error("default")
    
        assert (error_two != nil).inspect
        assert (error_two[:errors].is_a?(Array) == true)
    end
    
    
    test "Ensure we can get the response as json" do
        event = get_event
        code = 200
        body = { :key => "value" }
        response = Servicecall::Response.new(event, code, body)
        json = response.to_json
    
        assert (json.include?('"event":"user_get"') == true)
        assert (json.include?('"success":true') == true)
        assert (json.include?('"data":{"key":"value"}') == true)
    end
    
    
    test "Ensure we can get the body attributes by key" do
        event = get_event
        code = 200
        body = { :key => "value", :array => [] }
        response = Servicecall::Response.new(event, code, body)
    
        assert (response[:key] == "value")
        assert (response[:array].is_a?(Array))
    end
    
    
    test "Ensure we can parse a response" do
        net_response = CallResponse.new(
            200,
            { :event => get_event, :data => { :key => "value" } }.to_json
        )
        
        response = Servicecall::Response.parse(net_response)

        assert (response.instance_of?(Servicecall::Response) == true)
        assert (response.code == 200)
        assert (response.event == get_event)
        assert (response.body != nil)
        assert (response.body[:key] == "value")
        assert (response.body["key"] == "value")
    end
    
end
