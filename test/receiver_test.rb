require 'jwt'
require 'test_helper'

class Servicecall::ReceiverTest < ActiveSupport::TestCase
    
    
    test "Ensure we can create a new receiver instance" do
        receiver = new_instance
    
        assert (receiver.instance_of?(Servicecall::Receiver) == true)
    end
    
    
    test "Ensure we can receive a valid request" do
        receiver = new_instance
        request_data = Servicecall::Request.new(get_event, { :key => "value" }).to_json
        response = receiver.receive(get_jwt_token(receiver.config), request_data)
    
        assert (response.instance_of?(Servicecall::Response) == true)
        assert (response.was_successful? == true)
        assert (response.body != nil)
        assert (response.body[:user] != nil)
        assert (response.body[:user]["first_name"] == "John")
        assert (response.body[:user]["last_name"] == "Smith")
        assert (response.body[:user]["key"] == "value")
    end
    
    
    test "Ensure we can receive a invalid request and error" do
        receiver = new_instance
        request_data = Servicecall::Request.new("fake_event", { :key => "value" }).to_json
        response = receiver.receive(get_jwt_token(receiver.config), request_data)
    
        assert (response.instance_of?(Servicecall::Response) == true)
        assert (response.was_successful? == false)
        assert (response.error(nil) != nil)
        assert (response.error(nil)["message"] == "Invalid event")
    end
    
    
    test "Ensure we can receive a invalid request when the token is invalid" do
        receiver = new_instance
        request_data = Servicecall::Request.new(get_event, { :key => "value" }).to_json
        response = receiver.receive("invalid_token", request_data)
        
        assert (response.instance_of?(Servicecall::Response) == true)
        assert (response.was_successful? == false)
        assert (response.error(nil) != nil)
        assert (response.error(nil)["error"] == "Invalid token")
    end
    
    
    private
        def new_instance
            config = Servicecall::Configurator.new
            config.jwt_secret(jwt_secret)
            config.register_service(:test, "http://localhost:3000")
            config.on(get_event, "user#get")
            
            Servicecall::Receiver.new(config)
        end
    
    
end
