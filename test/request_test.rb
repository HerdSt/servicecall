require 'test_helper'

class Servicecall::RequestTest < ActiveSupport::TestCase
    
    
    test "Ensure we can create a request" do
        event = get_event
        body = get_body
        request = Servicecall::Request.new(event, body)
    
        assert (request.instance_of?(Servicecall::Request) == true)
        assert (request.event == event)
        assert (request.body.is_a?(Hash) == true)
        assert (request.body[:key] == "value")
        assert (request.body["key"] == "value")
    end
    
    
    test "Ensure we can convert a request to json" do
        event = get_event
        body = get_body
        request = Servicecall::Request.new(event, body)
        json = request.to_json
    
        assert (json.include?('"event":"user_get"') == true)
        assert (json.include?('"key":"value"') == true)
    end
    
    
    test "Ensure we can parse a request" do
        json_body = { :event => get_event, :data => get_body }.to_json
        request = Servicecall::Request.new(nil, nil)
        request.parse!(json_body)
        
        assert (request.event == get_event)
        assert (request.body.is_a?(Hash) == true)
        assert (request.body[:key] == "value")
        assert (request.body["key"] == "value")
    end
    
    
end
