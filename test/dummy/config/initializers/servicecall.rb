# Configures service call
Servicecall.setup do |config|

    config.jwt_secret "[-h9uhm8+B7\HU/k"
    
    config.register_service :test, "http://example.com"
    
    config.on("user_get", "user#get")

end
