class User
    include ActiveModel::Validations
    
    
    attr_accessor :first_name, :last_name
    
    validates :first_name, presence: true, length: { minimum: 1, maximum: 80 }
    validates :last_name, presence: true, length: { minimum: 1, maximum: 80 }
    
    
    def initialize(first_name, last_name)
        self.first_name = first_name
        self.last_name = last_name
    end

    
end
