class UserServiceEvent < Servicecall::Responder
    
    
    def get(data)
        {
            :user => {
                :first_name => "John",
                :last_name => "Smith"
            }.merge(data)
        }
    end
    
    
end
